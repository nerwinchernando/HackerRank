# > marks = {"Ramesh":23, "Vivek":40, "Harsh":88, "Mohammad":60}
# > group_by_marks(marks, 30)
# => {"Failed"=>[["Ramesh", 23]], "Passed"=>[["Vivek", 40], ["Harsh", 88], ["Mohammad", 60]]}
def group_by_marks(marks, pass_marks)
  # your code here
    marks.group_by do |key, value| value < pass_marks ? "Failed" : "Passed" end
end
# > strike("Meow!") # => "<strike>Meow!</strike>"
# > strike("Foolan Barik") # => "<strike>Foolan Barik</strike>"
# > mask_article("Hello World! This is crap!", ["crap"])
# "Hello World! This is <strike>crap</strike>!"
def mask_article(text, masks)
  new_str = text
  masks.each do |mask|
   new_str.gsub!(mask, strike(mask)) if  new_str.include?(mask)
  end
  return new_str
end

def strike(text)
 "<strike>#{text}</strike>"
end
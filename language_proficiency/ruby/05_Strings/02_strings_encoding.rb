# str = "With ♥!"
# print("My String's encoding: ", str.encoding.name) 
# print("\nMy String's size: ", str.size)
# print("\nMy String's bytesize: ", str.bytesize)
# produces this output:

# My String's encoding: UTF-8
# My String's size: 7
# My String's bytesize: 9

def transcode(str)
  return str if str.encoding.name == "UTF-8"
  str.force_encoding(Encoding::UTF_8)
end
'''John's clothing store has a pile of  loose socks where each sock  is labeled with an integer, , denoting its color. He wants to sell as many socks as possible, but his customers will only buy them in matching pairs. Two socks,  and , are a single matching pair if .

Given  and the color of each sock, how many pairs of socks can John sell?

Input Format

The first line contains an integer, , denoting the number of socks. 
The second line contains  space-separated integers describing the respective values of .

Constraints

Output Format

Print the total number of matching pairs of socks that John can sell.

Sample Input

9
10 20 20 10 10 30 50 10 20
Sample Output

3
Explanation

sock.png

As you can see from the figure above, we can match three pairs of socks. Thus, we print  on a new line.

Submissions: 5693
Max Score: 10
Difficulty: Easy
Rate This Challenge:
     Thanks!
How can we improve?
Let us know
More
'''


#!/bin/python3

import sys
from itertools import groupby

def sock_merchant(n,c):
    number_of_socks = 0
    c.sort()
    sock_lists = [list(j) for i, j in groupby(c)]
    for sock in sock_lists:
        number_of_socks = number_of_socks + int(sock.__len__() / 2)
    print(number_of_socks)

n = int(input().strip())
c = [int(c_temp) for c_temp in input().strip().split(' ')]

sock_merchant(n,c)
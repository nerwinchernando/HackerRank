/* Weather Observation Station 7

Query the list of CITY names ending with vowels (a, e, i, o, u) from STATION. Your result cannot contain duplicates.

*/

/* MYSQL */
SELECT distinct(city) 
FROM station 
WHERE right(city,1) in ('a','e','i','o','u');

/* Oracle */
SELECT distinct(city) 
FROM station 
WHERE SUBSTR(city,-1, 1) in ('a','e','i','o','u');

/* MS SQL SERVER */
SELECT distinct(city) 
FROM station 
WHERE right(city,1) in ('a','e','i','o','u');



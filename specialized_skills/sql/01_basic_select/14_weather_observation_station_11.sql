/*
Query the list of CITY names from STATION that either do not start with vowels or do not end with vowels. Your result cannot contain duplicates.
*/

/* MySQL */
SELECT DISTINCT city
FROM station 
WHERE SUBSTRING(city,1,1) not in ('A','E','I','O','U') or
    SUBSTRING(city,-1,1) not in ('A','E','I','O','U');

/* Oracle */
SELECT DISTINCT(city)
FROM station 
WHERE UPPER(SUBSTR(city,1,1)) not in ('A','E','I','O','U') or
    UPPER(SUBSTR(city,-1,1)) not in ('A','E','I','O','U');

/* MS SQL SERVER */
SELECT DISTINCT CITY 
FROM STATION  
WHERE CITY NOT LIKE '[AEIOU]%' OR CITY NOT LIKE '%[aeiou]';

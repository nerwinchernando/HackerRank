/* 
  Query the list of CITY names from STATION that do not start with vowels and do not end with vowels. Your result cannot contain duplicates. 
*/

/* MySQL MS SQL SERVER */
SELECT DISTINCT city
FROM station 
WHERE UPPER(LEFT(city,1)) not in ('A','E','I','O','U') AND
    UPPER(RIGHT(city,1)) not in ('A','E','I','O','U');

/* ORACLE */
SELECT DISTINCT city
FROM station 
WHERE UPPER(SUBSTR(city,1,1)) not in ('A','E','I','O','U') AND
    UPPER(SUBSTR(city,-1, 1)) not in ('A','E','I','O','U');

/* 
  Query the list of CITY names from STATION that do not end with vowels. Your result cannot contain duplicates.
*/

/* MySQL & MS SQL Server */
SELECT distinct(city) 
FROM station 
WHERE right(city,1) not in ('a','e','i','o', 'u')

/* ORACLE */
SELECT distinct(city) 
FROM station 
WHERE LOWER(SUBSTR(city,-1, 1)) not in ('a','e','i','o','u');

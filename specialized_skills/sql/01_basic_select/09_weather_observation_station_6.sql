/* Weather Observation Station 6

Query the list of CITY names starting with vowels (i.e., a, e, i, o, or u) 
from STATION. Your result cannot contain duplicates.
*/
/* MySQL */
SELECT distinct(city) 
FROM station 
WHERE substring(city,1,1) in ('a','e','i','o','u');

/* Oracle */
SELECT DISTINCT(city)
FROM station 
WHERE UPPER(SUBSTR(city,1,1)) in ('A','E','I','O','U');

/* MS SQL SERVER */
SELECT DISTINCT(city)
FROM station 
WHERE city LIKE '[AEIOU]%';

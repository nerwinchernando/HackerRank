/*
 Problem: Write a query that prints a list of employee names (i.e.: the name attribute) from the Employee table in alphabetical order.
*/

SELECT Name
FROM employee
ORDER BY Name ASC;

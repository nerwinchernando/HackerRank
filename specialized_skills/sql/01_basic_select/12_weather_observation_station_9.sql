/* Weather Observation Station 9

Query the list of CITY names from STATION that do not start with vowels. Your result cannot contain duplicates.

*/

/* MySQL & MS SQL Server */
SELECT distinct(city) 
FROM station 
WHERE left(city,1) not in ('a','e','i','o','u');

/* Oracle */
SELECT distinct(city) 
FROM station 
WHERE LOWER(SUBSTR(city,1, 1)) not in ('a','e','i','o','u');

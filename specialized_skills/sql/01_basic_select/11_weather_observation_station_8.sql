/*
Weather Observation Station 8

Query the list of CITY names from STATION which have vowels (i.e., a, e, i, o, and u) 
as both their first and last characters. Your result cannot contain duplicates.
*/
/* MySQL */
SELECT distinct(city) 
FROM station 
WHERE right(city,1) in ('a','e','i','o','u') AND left(city,1) in ('a','e','i','o','u');

/* ORACLE */
SELECT distinct(city) 
FROM station 
WHERE LOWER(SUBSTR(city,1, 1)) in ('a','e','i','o','u') and LOWER(SUBSTR(city,-1, 1)) in ('a','e','i','o','u') ;

/* MS SQL SERVER */
SELECT distinct(city) 
FROM station 
WHERE right(city,1) in ('a','e','i','o','u') AND left(city,1) in ('a','e','i','o','u');
